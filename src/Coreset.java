import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;


class ACost {
    double cost;
}

public class Coreset {
    // reset point weights
    public static void resetWeights(Point[] points) {
        for (Point p : points) {
            p.weight = 1;
        }
    }

    // distance from point to point set
    public static double distPS(Point p, Point[] points) {
        double minDist = Double.MAX_VALUE;
        for (Point q : points) {
            double dist = KMeans.distPP(p, q);
            if (dist < minDist) {
                minDist = dist;
            }
        }

        return minDist;
    }

    // cost of a clustering
    public static double cost(Point[] P, Point[] C) {
        double cost = 0;
        for (Point p : P) {
            cost += p.weight * Math.pow(distPS(p, C), 2);
        }
        return cost;
    }

    // dynamic for best cost
    public static void bestCostFor(Point[] points, int level, int[] indices, int k, ACost cost) {
        if (level == k) {
            bestCost(points, cost, indices);
        } else {
            int newLevel = level + 1;
            int[] newIndices = new int[newLevel];
            System.arraycopy(indices, 0, newIndices, 0, level);
            newIndices[level] = 0;
            while (newIndices[level] < points.length) {
                bestCostFor(points, newLevel, newIndices, k, cost);
                newIndices[level]++;
            }
        }
    }

    // test for best cost with centroids in point set
    public static void bestCost(Point[] points, ACost cost, int[] indices) {
        Point[] centroids = new Point[indices.length];
        for (int i = 0; i < indices.length; i++) {
            centroids[i] = points[indices[i]];
        }
        double newCost = cost(points, centroids);
        if (newCost < cost.cost) {
            cost.cost = newCost;
        }
    }

    // sum of weights
    public static double weightSum(ArrayList<Point> points) {
        double sum = 0;
        for (Point p : points) {
            sum += p.weight;
        }
        return sum;
    }

    // logarithm base 2
    public static double log2(double x) {
        return Math.log(x) / Math.log(2);
    }

    // check whether point lies in ball 2 \ ball 1 centered at c
    public static boolean inAnnulus(Point p, Point c, double r1, double r2) {
        double dist = KMeans.distPP(p, c);
        if (r1 <= dist && dist < r2) {
            return true;
        } else {
            return false;
        }
    }

    // calculate everything for the current cell
    private static void handleCell(int[] indices, ArrayList<Point> unvisited, int d, Point centroid,
                                   double rad, double x, int z_, double r, ArrayList<Point> coreset) {
        // check for all unvisited points if they are in the cell
        Random rnd = new Random();
        ArrayList<Point> inCell = new ArrayList<>();

        // set top left coordinates of the cell
        double[] topLeft = new double[d];
        for (int i = 0; i < d; i++) {
            topLeft[i] = centroid.cos[i] - rad + indices[i] * x;
        }

        // for each unrepresented point
        for (Point p : unvisited) {
            // check if the point is in the cell
            boolean inC = true;
            for (int c = 0; c < d; c++) {
                if (topLeft[c] > p.cos[c] || p.cos[c] >= topLeft[c] + x) {
                    inC = false;
                    break;
                }
            }

            // if in cell and in ball, group the point
            if (inC && inAnnulus(p, centroid,
                    (z_ == 0) ? 0 : Math.pow(2, z_ - 1) * r,
                    Math.pow(2, z_) * r)) {
                inCell.add(p);
            }
        }

        // create rep of all points in the cell
        if (!inCell.isEmpty()) {
            // pick random point in cell as representative
            Point rep = inCell.get(rnd.nextInt(inCell.size()));
            // rep.cluster = -1;
            rep.weight = weightSum(inCell);

            // add point to coreset
            coreset.add(rep);

            // remove points from set
            for (Point p : inCell) {
                unvisited.remove(p);
                if (p != rep) {
                    p.weight = 0;
                }
            }
        }
    }

    // recursively iterate over dimensions
    private static void _nFor(int level, int[] indices, int d, int lo, double hi, ArrayList<Point> unvisited,
                              Point centroid, double rad, double x, int z_, double r, ArrayList<Point> coreset) {
        if (level == d) {
            handleCell(indices, unvisited, d, centroid, rad, x, z_, r, coreset);
        } else {
            int newLevel = level + 1;
            int[] newIndices = new int[newLevel];
            System.arraycopy(indices, 0, newIndices, 0, level);
            newIndices[level] = lo;
            while (newIndices[level] < hi) {
                _nFor(newLevel, newIndices, d, lo, hi, unvisited, centroid, rad, x, z_, r, coreset);
                newIndices[level]++;
            }
        }
    }

    // compute a coreset for P
    public static Point[] computeCoreset(Point[] points, int k, double eps, double a, Point[] centroids) {
        // init algorithm
        int n = points.length;
        int d = points[0].cos.length;
        ArrayList<Point> unvisited = new ArrayList<>(Arrays.asList(points));

        // compute z
        double z = log2(n) * log2(a * log2(n));

        // init coreset to empty set
        ArrayList<Point> coreset = new ArrayList<>();

        // compute centroids with k-means++
        if (centroids == null) {
            centroids = KMeans.computeClustering(points, k);
        }

        // compute r
        double r = Math.sqrt(cost(points, centroids) / (a * log2(n) * n));

        // for annulus z_
        for (int z_ = 0; z_ <= z; z_++) {
            // outer radius of this annulus
            double rad = Math.pow(2, z_) * r;

            // compute grid cell side lengths
            double x = (eps * rad) / Math.sqrt(d);

            // number of cells per row
            double y = 2 * rad / x;

            // for each centroid
            for (int i = 0; i < k; i++) {
                // d nested for loop handling each cell in the grid
                _nFor(0, new int[0], d, 0, y, unvisited, centroids[i], rad, x, z_, r, coreset);
            }
        }

        Point[] ret = new Point[coreset.size()];
        ret = coreset.toArray(ret);
        return ret;
    }

    // compute union of coreset
    public static Point[] coresetUnion(Point[] A, Point[] B) {
        Point[] union = new Point[A.length + B.length];
        System.arraycopy(A, 0, union, 0, A.length);
        System.arraycopy(B, 0, union, A.length, B.length);
        return union;
    }

    // compute MPC coreset for P
    public static Point[] mpcCoreset(Point[] points, int k, double eps, double a, Point[] centroids) {
        // set number of machines
        int n = points.length;
        int nMachines = (int) Math.floor(Math.sqrt(n));

        // init machines
        ArrayList<ArrayList<Point>> tempMachines = new ArrayList<>();
        for (int i = 0; i < nMachines; i++) {
            tempMachines.add(new ArrayList<>());
        }

        // add point to each machine
        for (int i = 0; i < n; i++) {
            tempMachines.get(i % nMachines).add(points[i]);
        }

        // transform into point arrays
        Point[][] machines = new Point[nMachines][];
        for (int i = 0; i < nMachines; i++) {
            machines[i] = new Point[tempMachines.get(i).size()];
            machines[i] = tempMachines.get(i).toArray(machines[i]);
        }

        // compute coresets in parallel
        double level = 1;
        while (machines.length > 1) {
            int t = machines.length;

            // compute coresets of points on machines
            for (int i = 0; i < t; i++) {
                // compute coreset with new eps
                machines[i] = computeCoreset(machines[i], k, eps / (level), a, (level == 1) ? centroids : null);
            }

            // combine 2 coresets into single machine
            int _t = (int) Math.ceil((double) t / 2);
            for (int i = 0; i < _t; i++) {
                if (2 * i + 1 < t) {
                    machines[i] = coresetUnion(machines[2 * i], machines[2 * i + 1]);
                } else {
                    machines[i] = machines[2 * i];
                }
            }
            Point[][] temp = new Point[_t][];
            System.arraycopy(machines, 0, temp, 0, _t);
            machines = temp;

            level++;
        }

        return computeCoreset(machines[0], k, eps / (level), a, null);
    }
}