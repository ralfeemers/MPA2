import java.io.IOException;
import java.util.Random;

public class Main {
    public static Point[] randomPoints(int n, int d) {
        Random rnd = new Random();
        Point[] points = new Point[n];
        for (int i = 0; i < n; i++) {
            double[] cos = new double[d];
            for (int j = 0; j < d; j++) {
                cos[j] = rnd.nextDouble();
            }
            points[i] = new Point(cos, -1);
        }

        return points;
    }

    // get best cost with centroids in the point set
    public static void bestCost(Point[] points, int k) {
        ACost cost = new ACost();
        cost.cost = Double.MAX_VALUE;
        Coreset.bestCostFor(points, 0, new int[0], k, cost);
        System.out.println(cost.cost);
    }

    // best cost with multiple runs of kMeans
    public static Point[] bestKMeans(Point[] points, int k) {
        Point[] centroids = new Point[k];
        double cost = Double.MAX_VALUE;
        // try k times
        for (int i = 0; i < k; i++) {
            Point[] newCentroids = KMeans.computeClustering(points, k);
            double newCost = Coreset.cost(points, newCentroids);
            if (newCost < cost) {
                centroids = newCentroids;
                cost = newCost;
            }
        }
        return centroids;
    }

    // run single instance of the coreset problem
    public static void runCoreset(String _fileName, int k, double a, double eps) {
        String fileName = "data\\" + _fileName + ".txt";
        _fileName +=  "_" + (int) (100 * eps);

        // timer
        double timer = System.currentTimeMillis();

        // read points
        Point[] points = IO.readPoints(fileName);

        // compute initial centroids
        Point[] centroids = bestKMeans(points, k);
        double actualCost = Coreset.cost(points, centroids);
        double time = System.currentTimeMillis();
        IO.writePoints(points, actualCost,0, (time - timer) / 1000,
                "output\\" + _fileName + "_points.txt");
        timer = time;

        // compute MPC coreset
        Coreset.resetWeights(points);
        Point[] mpc = Coreset.mpcCoreset(points, k, eps, a, centroids);
        double mpcCost = Coreset.cost(mpc, centroids);
        time = System.currentTimeMillis();
        IO.writePoints(mpc, mpcCost, Math.abs(actualCost - mpcCost) / points.length,
                (time - timer) / 1000, "output\\" + _fileName + "_mpc.txt");

        // run python script to plot graph
        try {
            Runtime.getRuntime().exec("python plotter.py " + _fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void run() {
        // input values
        String[] fileNames = new String[] {"locations"};
        int[] ks = new int[] {11, 15, 20, 5};
        double[] epss = new double[] {.99, .75, .5, .25, .1};
        double a = 10;
        for (double eps : epss) {
            for (int i = 0; i < fileNames.length; i++) {
                runCoreset(fileNames[i], ks[3], a, eps);
            }
        }
    }

    public static void main(String[] args) {
        run();
    }
}
