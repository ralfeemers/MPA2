import java.io.*;
import java.util.Scanner;

public class IO {
    public static void writePoints(Point[] points, double cost, double error, double duration, String fileName) {
        try {
            FileWriter writer = new FileWriter(fileName);
            writer.write("Points: " + points.length + ", Cost: " + cost + ", Average error: " + error +
                    ", Duration (s): " + duration + "\n");
            for (Point p : points) {
                writer.write(p.toString() + "\n");
            }
            writer.close();
            System.out.println("Wrote " + points.length + " points to " + fileName +  " to file");
        } catch (IOException e) {
            System.out.println("An error occurred when writing to file.");
            e.printStackTrace();
        }
    }

    public static Point[] normalize(Point[] input, int d) {
        // normalizing
        double[] mins = new double[d];
        double[] maxs = new double[d];

        for (int i = 0; i < d; i++) {
            mins[i] = Double.MAX_VALUE;
            maxs[i] = -Double.MAX_VALUE;
        }

        // find min and max for each dimension
        for (Point p : input) {
            for (int i = 0; i < d; i++) {
                if (p.cos[i] < mins[i]) {
                    mins[i] = p.cos[i];
                }
                if (p.cos[i] > maxs[i]) {
                    maxs[i] = p.cos[i];
                }
            }
        }

        for (Point p : input) {
            for (int i = 0; i < d; i++) {
                p.cos[i] = (p.cos[i] - mins[i]) / (maxs[i] - mins[i]);
            }
        }

        return input;
    }

    public static Point[] readPoints(String fileName) {
        try {
            // scanner object
            Scanner reader = new Scanner(new File(fileName));

            // get nPoints and dimensions
            String firstLine = reader.nextLine();
            String[] split = firstLine.split("\\s+");
            Point[] input = new Point[Integer.parseInt(split[0])];
            int d = Integer.parseInt(split[1]);

            // add points to input point set
            for (int i = 0; i < input.length; i++) {
                split = reader.nextLine().split("\\s+");
                double[] cos = new double[d];
                for (int j = 0; j < d; j++) {
                    // filtering dumb formatting some datasets may have
                    String dumbFormat = split[j];
                    String[] parts = dumbFormat.split("\\.");
                    if (parts.length > 1) {
                        split[j] = parts[0] + ".";
                        for (int k = 1; k < parts.length; k++) {
                            parts[k] = parts[k].replace(".", "");
                            split[j] += parts[k];
                        }
                    }

                    cos[j] = Double.parseDouble(split[j]);
                }
                input[i] = new Point(cos, -1);
            }

            return normalize(input, d);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }
}
