import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class Point {
    double[] cos;
    int cluster;
    double weight;

    public Point(double[] cos, int cluster) {
        this.cos = cos;
        this.cluster = cluster;
        weight = 1;
    }

    public static Point addPoints(Point p, Point q) {
        if (p == null && q == null) {
            return null;
        } else if (p == null) {
            return q.copy(q.cluster);
        } else if (q == null) {
            return p.copy(p.cluster);
        }

        double[] sumCos = new double[p.cos.length];
        for (int i = 0; i < sumCos.length; i++) {
            sumCos[i] = p.cos[i] + q.cos[i];
        }
        return new Point(sumCos, p.cluster);
    }

    public static Point div(Point p, double divisor) {
        double[] divCos = new double[p.cos.length];
        for (int i = 0; i < divCos.length; i++) {
            divCos[i] = p.cos[i] / divisor;
        }
        return new Point(divCos, p.cluster);
    }

    public Point copy(int cluster) {
        return new Point(cos, cluster);
    }

    @Override
    public String toString() {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
        nf.setMaximumFractionDigits(9);
        DecimalFormat df = (DecimalFormat)nf;
        df.setGroupingUsed(false);
        String s = cluster + " ";
        for (double co : cos) {
            s += df.format(co) + " ";
        }
        s = s.substring(0, s.length() - 1);
        return s;
    }
}
