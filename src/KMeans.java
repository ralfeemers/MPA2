import java.util.Random;

public class KMeans {
    // distance from point to point
    public static double distPP(Point p, Point q) {
        double sum = 0;
        int d = Math.max(p.cos.length, q.cos.length);
        for (int i = 0; i < d; i++) {
            sum += Math.pow(p.cos[i] - q.cos[i], 2);
        }

        return Math.sqrt(sum);
    }

    // check if points are equal
    public static boolean equalsPP(Point p, Point q) {
        if (p == null || q == null) {
            return false;
        }

        if (p == q) {
            return true;
        }

        for (int i = 0; i < p.cos.length; i++) {
            if (Double.compare(p.cos[i], q.cos[i]) != 0) {
                return  false;
            }
        }

        return true;
    }

    // check if point set equals point set
    public static boolean equalsSS(Point[] A, Point[] B) {
        if (A == null || B == null) {
            return false;
        }

        if (A == B) {
            return true;
        }

        for (int i = 0; i < A.length; i++) {
            if (!equalsPP(A[i], B[i])) {
                return false;
            }
        }
        return true;
    }

    // init centroids via k-means++
    public static Point[] initCentroids(Point[] points, int k) {
        // init vars
        Random rnd = new Random();
        Point[] centroids = new Point[k];
        int n = points.length;

        // random number between 0 and 1
        double x = rnd.nextDouble();
        int sampleIndex = (int) Math.floor(x * n);
        centroids[0] = points[sampleIndex].copy(0);
        double[] minDistances = new double[n];
        for (int i = 0; i < n; i++) {
            minDistances[i] = Double.MAX_VALUE;
        }

        // run k-means
        for (int i = 1; i < k; i++) {
            // update dists
            for (int j = 0; j < n; j++) {
                double dist = distPP(points[j], centroids[i - 1]);
                if (dist < minDistances[j]) {
                    minDistances[j] = dist;
                }
            }

            // compute cumulative distribution of squared distances
            double[] cumulative = new double[n];
            cumulative[0] = Math.pow(minDistances[0], 2);
            for (int j = 1; j < n; j++) {
                cumulative[j] = cumulative[j - 1] + Math.pow(minDistances[j], 2);
            }

            // sample next centroid
            x = rnd.nextDouble() * cumulative[n - 1];
            if (x <= cumulative[0]) {
                sampleIndex = 0;
            } else {
                for (int j = 1; j < n; j++) {
                    if (x > cumulative[j - 1] && x <= cumulative[j]) {
                        sampleIndex = j;
                    }
                }
            }

            // add sampled point to centroids
            centroids[i] = points[sampleIndex].copy(i);
        }

        return centroids;
    }

    // compute cluster labels
    public static void computeLabels(Point[] points, Point[] centroids) {
        int n = points.length;
        int k = centroids.length;
        for (int i = 0; i < n; i++) {
            double minDist = Double.MAX_VALUE;
            for (int j = 0; j < k; j++) {
                double currentDist = distPP(points[i], centroids[j]);
                if (currentDist < minDist) {
                    minDist = currentDist;
                    points[i].cluster = j;
                }
            }
        }
    }

    // compute cluster labels
    public static Point[] computeCentroids(Point[] points, int k) {
        int n = points.length;
        double[] clusterSizes = new double[k];

        // init point sum
        Point[] pointSum = new Point[k];

        // fill sizes and sums
        for (int i = 0; i < n; i++) {
            clusterSizes[points[i].cluster] += 1;
            pointSum[points[i].cluster] = Point.addPoints(pointSum[points[i].cluster], points[i]);
        }

        // compute mean
        Point[] centroids = new Point[k];
        for (int i = 0; i < k; i++) {
            centroids[i] = Point.div(pointSum[i], clusterSizes[i]);
        }

        return centroids;
    }

    // k-means clustering
    public static Point[] computeClustering(Point[] points, int k) {
        Point[] centroids = initCentroids(points, k);
        computeLabels(points, centroids);
        Point[] newCentroids = computeCentroids(points, k);
        while (!equalsSS(centroids, newCentroids)) {
            centroids = newCentroids;
            computeLabels(points, centroids);
            newCentroids = computeCentroids(points, k);
        }

        return centroids;
    }
}
