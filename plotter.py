import matplotlib.pyplot as plt
import math
import sys

files = ["output\\{}_points.txt".format(sys.argv[1]), "output\\{}_mpc.txt".format(sys.argv[1])]
sets = []
for f in files:
    f = open(f, "r")
    clusters = {}
    skip = True
    for l in f:
        if not skip:
            cluster = l[0:l.find(" ")]
            point = l[l.find(" ") + 1:-1]
            point = point.split()
            cos = [float(co) for co in point]
            if cluster not in clusters:
                clusters[cluster] = [cos]
            else:
                clusters[cluster].append(cos)
        else:
            skip = False
    f.close()
    sets.append(clusters)

fig, axs = plt.subplots(1, len(files), sharex=True, sharey=True)
fig.set_size_inches(6 * len(files), 5)
for i in range(len(sets)):
    n = 0
    for k in sets[i].keys():
        n += len(sets[i][k])
    for k in sets[i].keys():
        x = [co[0] for co in sets[i][k]]
        y = [co[1] for co in sets[i][k]]
        axs[i].scatter(x, y, s=1500 / n)
        axs[i].set_title(files[i])

plt.savefig("figures\\{}.png".format(sys.argv[1]))
plt.close()

